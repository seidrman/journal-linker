document.addEventListener('DOMContentLoaded', function(tab) {
	today = new Date(Date.now())
	
	//! Martedì 26 Maggio 2020
	//__[Team Antani | https://7pixel.myjetbrains.com/youtrack/issues?q=commented:%202020-05-26%20commenter:antani%20or%20created:%202020-05-26%20created%20by:antani]__ 

	dayName = today.toLocaleString('it', { weekday: 'long' });
	dayName = dayName.replace(/^./, dayName[0].toUpperCase())

	monthName = today.toLocaleString('it', { month: 'long' });
	monthName = monthName.replace(/^./, monthName[0].toUpperCase())

	journalDate = today.toISOString().substring(0, 10);
    
	journalLink=`! ${dayName} ${today.getDate()} ${monthName} ${today.getFullYear()} \r\n`
	journalLink += `__[Team Antani | https://7pixel.myjetbrains.com/youtrack/issues?q=commented:%20${journalDate}%20commenter:antani%20or%20created:%20${journalDate}%20created%20by:antani]__`

    status = copyTextToClipboard(journalLink);

	if (status != 'successful') {
		document.getElementById("text").classList.add('unsuccess');
		document.getElementById("text").innerHTML = "Something went wrong!"
	} else {
		document.getElementById("text").classList.add('success');
		document.getElementById("text").innerHTML = "Link has been copied to clipboard!"
	}
	setTimeout(function() {
		window.close();
	}, 1500);
}, false);

function formatLink(title, url) {
    title = title.substring(0, title.lastIndexOf(" ")); //remove id
    title = title.substring(0, title.lastIndexOf(" ")); //remove ':'

    return '[' + title + ' | ' + url + ']'
}

function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        var msg = 'error'
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
    return msg
}
